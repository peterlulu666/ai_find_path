Name: Yanzhi Wang
UTA ID: 1001827416
Programming language is Python 3
It cannot run in omega
How the code is structured:
1. Build up the node. We only keep minimum requirement.
So in UCS, the node only contain parent state, this state, level, cumulative_cost.
In A* search, the heuristic and the f(n) is going to be used.
2. The uninformed_search is using UCS
2.1 Build up the graph.
we are going to use defaultdict to store graph.
The key is the city and the value is its adjacent city and path cost.
The type of key is string and the value is list.
2.2 Build up fringe and closed set. The fringe is PriorityQueue. The priority is based on cumulative_cost.
2.3 Build up starting node.
2.4 Add the starting node to fringe.
2.5 We implement UCS with graph search.
2.5.1 In the UCS, we pop the node from fringe.
2.5.2 Then we check if it the goal, if yes, break the while loop.
2.5.3 Then we check if it is in the closed set.
2.5.4 If it is in the closed set, we look at the next one.
2.5.5 If it is not in the closed set, we add the state to the closed set and we We expand the node.
2.5.6 We count expanded node between pop node and check goal node.
2.5.7 We count generated node before add generated node to fringe.
3. The informed_search is A* search.
3.1 We change something in uninformed_search.
3.2 The only thing we have to change is building up the heuristic value dict and the g(n) is replaced by f(n).
So in the PriorityQueue, the priority is based on f(n).
4. print_info.
4.1 If goal found, we print nodes expanded, nodes generated, distance, and route.
If goal not found, we print nodes expanded and nodes generated.
About printing route:
goal_node pointing to parent_node_A pointing to parent_node_B pointing to start_node
we store their state in list [goal, parent_A, parent_B, start]
reverse list, then the list is [start, parent_B, parent_A, goal]. It is the route from start to goal
About printing distance:
the total distance is the cumulative path cost of goal
the node_A to node_B distance is
goal node cumulative path cost subtract parent_node_A cumulative path cost = distance1
parent_node_A cumulative path cost subtract parent_node_B cumulative path cost = distance2
parent_node_B cumulative path cost subtract start node cumulative path cost = distance3
store the distance to list [distance1, distance2, distance3]
reverse list. Each distance is the city to city distance. The sequence is from start to goal
How to run the code:
1. If you are running Uninformed search
python3 find_path.py input1.txt origin_city destination_city
1.1 An example
python3 find_path.py input1.txt Bremen Kassel
2. If you are running Informed search
python3 find_path.py input1.txt origin_city destination_city h_kassel.txt
2.1 An example
python3 find_path.py input1.txt Bremen Kassel h_kassel.txt


