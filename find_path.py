from collections import defaultdict
from queue import PriorityQueue
import sys


# Build up the node class
# We only keep minimum requirement
# So the node only contain parent state, this state, level, cumulative_cost
# In A* search, the heuristic and the fn is going to be used
class Node:
    def __init__(self, parent, state, level, cumulative_cost, heuristic=None):
        # this node is pointing to the prev node
        self.parent = parent
        # The name of city
        self.state = state
        # The depth of the node
        self.level = level
        # It is the g(n)
        # It is the actual cost of the path from the root to n (i.e., g(n) is the sum of
        # the costs of all edges traversed to get from the root to n)
        self.cumulative_cost = cumulative_cost
        # It is the h(n)
        self.heuristic = heuristic

    def get_fn(self):
        return self.cumulative_cost + int(self.heuristic)


def uninformed_search(file_name, source, goal):
    # Build up the graph
    # we are going to use defaultdict to store graph
    # The key is the city and the value is its adjacent city and path cost
    # The type of key is string and the value is list
    # So we pass list to defaultdict(), and store this in graph
    # This tells the interpreter that graph will hold a dictionary with values that are list
    # reference
    # https://tomron.net/2014/08/25/setdefault-vs-get-vs-defaultdict/
    graph = defaultdict(list)
    file = open(file_name, "r")
    for line in file:
        if "END OF INPUT" in line:
            break
        # It is the node in graph
        first_city = line.split()[0]
        # It is its successor
        second_city = line.split()[1]
        # It is the distance between the node and its successor
        successor_path_cost = line.split()[2]
        # Build up the graph
        # Its default value is the empty list
        # So we can append the [successor node and successor distance] to it
        graph[first_city].append([second_city, successor_path_cost])
        graph[second_city].append([first_city, successor_path_cost])

    # This is the graph
    # defaultdict(<class 'list'>,
    # {'Luebeck': [['Hamburg', '63']],
    # 'Hamburg': [['Luebeck', '63'], ['Bremen', '116'], ['Hannover', '153'], ['Berlin', '291']] ...
    # print(graph)

    # Build up fringe and closed set
    fringe = PriorityQueue()
    closed_set = set()

    # Build up starting node
    cumulative_path_cost = 0
    level = 0
    node = Node(None, source, level, cumulative_path_cost)

    # Add the starting node to fringe
    # https://stackoverflow.com/questions/9289614/how-to-put-items-into-priority-queues
    fringe.put((cumulative_path_cost, node))

    # Add all generated node, then we count it
    count_generated_node = []
    # Add all expanded node, then we count it
    count_expanded_node = []

    # In the UCS, we pop the node from fringe,
    # then we check if it the goal, if yes, break the while loop
    # then we check if it is in the closed set.
    # If it is in the closed set, we look at the next one
    # If it is not in the closed set, we add the state to the closed set and we We expand the node
    while fringe:
        if fringe.empty():
            # If goal not found
            print_info(count_expanded_node, count_generated_node)
            exit(0)

        # What node is in the fringe
        # print("What node is in the fringe ")
        # for tup in fringe.queue:
        #     print(tup[1].state)
        #     print(tup[1].parent)
        #     print(tup[1].level)
        #     print(tup[1].cumulative_cost)
        #     print("\n")

        # Pop node from fringe
        node = fringe.get()[1]
        # At this point we count the expanded node
        # It is the node poped
        # We do not expand all the poped node
        count_expanded_node.append(node)

        # What is the poped node
        # print("What is the poped node" + node.state)
        # print(node.state)
        # print(node.parent)
        # print(node.level)
        # print(node.cumulative_cost)
        # print("\n")

        # Check if the node.state is the goal
        if node.state == goal:
            # If goal found
            print_info(count_expanded_node, count_generated_node, node)
            exit(0)

        # Check if the node.state is in closed set
        if node.state in closed_set:
            # If yes, we look at the next one
            continue
        else:
            # If no, we add the state to the closed set
            closed_set.add(node.state)

            # successor_list = []
            # for successor in graph[node.state]:
            #     successor_list.append(successor[0])

            # We can implement the list comprehensions to create the above list
            # Extract first item of each sublist
            # https://stackoverflow.com/questions/25050311/extract-first-item-of-each-sublist
            # https://www.geeksforgeeks.org/python-get-first-element-of-each-sublist/
            successor_list = [item[0] for item in graph[node.state]]

            # We expand the node
            # We create the generated_node instances for each generated node
            # We add them to the fringe
            for successor in successor_list:
                # At this point we count the generated node
                # We generate all its successor
                # So the number of generated node is the number of all its successor
                count_generated_node.append(successor)
                # It is the distance between the node and its successor
                # It is the second element in the inner list
                successor_path_cost = None
                for inner_list in graph[node.state]:
                    if inner_list[0] == successor:
                        successor_path_cost = inner_list[1]
                cumulative_path_cost = node.cumulative_cost + int(successor_path_cost)

                # Build up the generated node
                generated_node = Node(node, successor, node.level + 1, cumulative_path_cost)

                # Add the generated node to fringe
                fringe.put((cumulative_path_cost, generated_node))


# The only thing we have to change is
# building up the heuristic value dict and
# the g(n) is replaced by f(n)
# So in the PriorityQueue, the priority is based on f(n)
def informed_search(file_name, source, goal, heuristic_filename):
    # Build up the graph
    # It is the same as the uninformed_search
    graph = defaultdict(list)
    file = open(file_name, "r")
    for line in file:
        if "END OF INPUT" in line:
            break
        # It is the node in graph
        first_city = line.split()[0]
        # It is its successor
        second_city = line.split()[1]
        # It is the distance between the node and its successor
        successor_path_cost = line.split()[2]
        # Build up the graph
        # Its default value is the empty list
        # So we can append the [successor node and successor distance] to it
        graph[first_city].append([second_city, successor_path_cost])
        graph[second_city].append([first_city, successor_path_cost])
    file.close()

    # Build up the heuristic
    hn_dict = {}
    file = open(heuristic_filename, "r")
    for line in file:
        if "END OF INPUT" in line:
            break
        node, h_value = line.split()
        hn_dict[node] = h_value
    file.close()

    # Build up fringe and closed set
    fringe = PriorityQueue()
    closed_set = set()

    # Build up starting node
    # The node contain cumulative_path_cost and heuristic, then we can calculate f(n)
    cumulative_path_cost = 0
    level = 0
    heuristic = hn_dict[source]
    node = Node(None, source, level, cumulative_path_cost, heuristic)

    # Add the starting node to fringe
    # https://stackoverflow.com/questions/9289614/how-to-put-items-into-priority-queues
    # In the PriorityQueue, the priority is based on f(n)
    fringe.put((node.get_fn(), node))

    # Add all generated node, then we count it
    count_generated_node = []
    # Add all expanded node, then we count it
    count_expanded_node = []

    # In the UCS, we pop the node from fringe,
    # then we check if it the goal, if yes, break the while loop
    # then we check if it is in the closed set.
    # If it is in the closed set, we look at the next one
    # If it is not in the closed set, we add the state to the closed set and we We expand the node
    while fringe:
        if fringe.empty():
            # If goal not found
            print_info(count_expanded_node, count_generated_node)
            exit(0)

        # What node is in the fringe
        # print("What node is in the fringe ")
        # for tup in fringe.queue:
        #     print(tup[1].state)
        #     print(tup[1].parent)
        #     print(tup[1].level)
        #     print(tup[1].cumulative_cost)
        #     print("\n")

        # Pop node from fringe
        node = fringe.get()[1]
        # At this point we count the expanded node
        # It is the node poped
        # We do not expand all the poped node
        count_expanded_node.append(node)

        # What is the poped node
        # print("What is the poped node" + node.state)
        # print(node.state)
        # print(node.parent)
        # print(node.level)
        # print(node.cumulative_cost)
        # print("\n")

        # Check if the node.state is the goal
        if node.state == goal:
            # If goal found
            print_info(count_expanded_node, count_generated_node, node)
            exit(0)

        # Check if the node.state is in closed set
        if node.state in closed_set:
            # If yes, we look at the next one
            continue
        else:
            # If no, we add the state to the closed set
            closed_set.add(node.state)

            # successor_list = []
            # for successor in graph[node.state]:
            #     successor_list.append(successor[0])

            # We can implement the list comprehensions to create the above list
            # Extract first item of each sublist
            # https://stackoverflow.com/questions/25050311/extract-first-item-of-each-sublist
            # https://www.geeksforgeeks.org/python-get-first-element-of-each-sublist/
            successor_list = [item[0] for item in graph[node.state]]

            # We expand the node
            # We create the generated_node instances for each generated node
            # We add them to the fringe
            for successor in successor_list:
                # At this point we count the generated node
                # We generate all its successor
                # So the number of generated node is the number of all its successor
                count_generated_node.append(successor)
                # It is the distance between the node and its successor
                # It is the second element in the inner list
                successor_path_cost = None
                for inner_list in graph[node.state]:
                    if inner_list[0] == successor:
                        successor_path_cost = inner_list[1]
                cumulative_path_cost = node.cumulative_cost + int(successor_path_cost)

                heuristic = hn_dict[successor]

                # Build up the generated node
                generated_node = Node(node, successor, node.level + 1, cumulative_path_cost, heuristic)

                # Add the generated node to fringe
                fringe.put((generated_node.get_fn(), generated_node))


def print_info(count_expanded_node, count_generated_node, goal_node=None):
    # Print nodes expanded
    print("nodes expanded: ", end="")
    print(len(count_expanded_node))

    # Print nodes generated
    print("nodes generated: ", end="")
    # We have to count the root node
    # We have to add 1
    print(len(count_generated_node) + 1)

    # If goal not found
    if goal_node is None:
        print("distance: infinity")

        print("rout: ")
        print("none")

    # If goal found
    else:
        # Print distance
        print("distance: ", end="")
        print(float(goal_node.cumulative_cost), end="")
        print(" km")

        # Print the route
        route = []
        adjacent_city_distance = []

        # At this point it is goal to start
        # We add the goal state, then add all other parent state
        route.append(goal_node.state)
        # We add goal to parent distance, then add all other parent to parent distance
        adjacent_city_distance.append(goal_node.cumulative_cost - goal_node.parent.cumulative_cost)
        while goal_node.parent:
            # Add all other parent state
            goal_node = goal_node.parent
            route.append(goal_node.state)

            # Add all other parent to parent distance
            # The parent node of the root node is None
            if goal_node.parent is None:
                adjacent_city_distance.append(goal_node.cumulative_cost - 0)
            else:
                adjacent_city_distance.append(goal_node.cumulative_cost - goal_node.parent.cumulative_cost)

        # At this point it is start to goal
        route.reverse()
        adjacent_city_distance.reverse()

        print("route: ")
        for index in range(len(route) - 1):
            print(route[index] + " to " + route[index + 1], end="")
            print(", ", end="")
            print(float(adjacent_city_distance[index + 1]), end="")
            print(" km")


def main():
    if len(sys.argv) == 4:
        input_filename = sys.argv[1]
        origin_city = sys.argv[2]
        destination_city = sys.argv[3]
        uninformed_search(input_filename, origin_city, destination_city)
    elif len(sys.argv) == 5:
        input_filename = sys.argv[1]
        origin_city = sys.argv[2]
        destination_city = sys.argv[3]
        heuristic_filename = sys.argv[4]
        informed_search(input_filename, origin_city, destination_city, heuristic_filename)


main()
